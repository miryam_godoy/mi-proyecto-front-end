import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  usuario:string;

  constructor( public auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.isLoggedIn$ = this.auth.isLoggedIn;
    this.usuario = this.auth.getUsername();
    console.log(this.usuario);

  }


  salir(){

    this.auth.logout();
    this.router.navigateByUrl('/login');

  }




}
