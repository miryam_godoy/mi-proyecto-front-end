import { SubcategoriaModel } from './subcategoria.model';

export class PresentacionModel {
    idPresentacionProducto: number;
    tamanho: number;
    descripcion: string;
    nombre: string;
    idProducto: SubcategoriaModel;
    existenciaProducto:{
        precioVenta: number;
    }
}