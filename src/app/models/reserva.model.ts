import { UsuarioModel } from './usuario.model';
import { PacienteModel } from './paciente.model';
import { FichaClinicaModel } from './ficha.model';

export class ReservaModel{

    idReserva: number;
    fecha: string;
    horaInicio: string;
    horaFin: string
    fechaHoraCreacion: string;
    flagEstado: string
    flagAsistio: string;
    observacion: string;
    idFichaClinica: FichaClinicaModel;
    idCliente: PacienteModel;
    idEmpleado: UsuarioModel;
    fechaCadena: string;
    fechaDesdeCadena: string;
    fechaHastaCadena: string;
    horaInicioCadena: string;
    horaFinCadena: string

}