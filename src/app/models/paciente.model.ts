export class PacienteModel{
    idPersona: number;
    nombre: string;
    apellido: string;
    email: string;
    telefono: string;
    seguroMedico: string;
    seguroMedicoNumero: number;
    ruc: string;
    cedula: string;
    tipoPersona: string;
    idLocalDefecto: number;
    flagVendedor: string;
    observacion: string;
    tipoCliente: string;
    fechaHoraAprobContrato: Date;
    nombreCompleto: string;
    limiteCredito: number;
    fechaNacimiento: Date;
    soloProximosCumpleanhos: string;
    
}