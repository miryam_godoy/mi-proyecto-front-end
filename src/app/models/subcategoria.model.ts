import { CategoriaModel } from './categoria.model';

export class SubcategoriaModel{
    idTipoProducto: number;
    descripcion: string;
    flagVisible: string;
    idCategoria: CategoriaModel;
    fechaHoraCadena: string;
    
}