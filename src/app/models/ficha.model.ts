import { UsuarioModel } from './usuario.model';
import { PacienteModel } from './paciente.model';
import { SubcategoriaModel } from './subcategoria.model';

export class FichaClinicaModel{
    
            idFichaClinica: number;
            fechaHora: string;
            motivoConsulta: string;
            diagnostico: string;
            observacion: string;
            idEmpleado: UsuarioModel;
            idCliente: PacienteModel;
            idTipoProducto: SubcategoriaModel;
            fechaHoraCadena: string;
            fechaDesdeCadena: string;
            fechaHastaCadena: string;
            /* adicional */
            seleccionar = true;
            activar = false;
}