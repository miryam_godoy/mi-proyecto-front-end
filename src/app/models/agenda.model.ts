import { UsuarioModel } from './usuario.model';
import { PacienteModel } from './paciente.model';
import { ReservaModel } from './reserva.model';

export class HorarioAgendaModel{
    idPersonaHorarioAgenda: number;
    dia: number;
    horaApertura: string;
    horaCierre: string;
    intervaloMinutos: number;
    idEmpleado: UsuarioModel;
    idCliente: PacienteModel;
    idReserva: ReservaModel;
}