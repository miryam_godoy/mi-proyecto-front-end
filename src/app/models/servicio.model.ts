import { FichaClinicaModel } from './ficha.model';
import { UsuarioModel } from './usuario.model';
import { PacienteModel } from './paciente.model';
import { SubcategoriaModel } from './subcategoria.model';

export class ServicioModel{
    idServicio: number;
    flagEstado: string;
    fechaHora: string
    observacion: string;
    presupuesto: number;
    idFichaClinica: FichaClinicaModel;
    idEmpleado: UsuarioModel;
    idCliente: PacienteModel;
    idTipoProducto: SubcategoriaModel;
    fechaHoraCadena: string;
    fechaDesdeCadena: string;
    fechaHastaCadena: string;
    
}