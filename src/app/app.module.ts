import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RegistroComponent } from './pages/registro/registro.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { NuevaFichaComponent } from './pages/nueva-ficha/nueva-ficha.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { DatePipe } from '@angular/common';

import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';

import { NgxPaginationModule } from 'ngx-pagination';



import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


import { PagerService } from './services/index';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { CrearReservaComponent } from './pages/crear-reserva/crear-reserva.component';
import { ModificarServicioComponent } from './pages/modificar-servicio/modificar-servicio.component';
import { ServicioComponent } from './pages/servicio/servicio.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { SubcategoriaComponent } from './pages/subcategoria/subcategoria.component';
import { PresentacionProductoComponent } from './pages/presentacion-producto/presentacion-producto.component';
import { HorarioAtencionComponent } from './pages/horario-atencion/horario-atencion.component';
import { HorarioExepcionComponent } from './pages/horario-exepcion/horario-exepcion.component';
@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    NuevaFichaComponent,
    ClientesComponent,
    ReservaComponent,
    CrearReservaComponent,
    ModificarServicioComponent,
    ServicioComponent,
    CategoriaComponent,
    SubcategoriaComponent,
    PresentacionProductoComponent,
    HorarioAtencionComponent,
    HorarioExepcionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    FormsModule,
    NgxPaginationModule,
    
    
    
    
  ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [DatePipe, PagerService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }

