import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { AuthService } from 'src/app/services/auth.service';
import { FichaClinicaModel } from 'src/app/models/ficha.model';


import { MatPaginator } from '@angular/material';
import { PacienteModel } from 'src/app/models/paciente.model';
import Swal from 'sweetalert2';
import { ServicioModel } from 'src/app/models/servicio.model';
import { CategoriaModel } from 'src/app/models/categoria.model';



@Component({
  selector: 'app-nueva-ficha',
  templateUrl: './nueva-ficha.component.html',
  styleUrls: ['./nueva-ficha.component.css']
})
export class NuevaFichaComponent implements OnInit {
  page:number = 1;
  p:number=1;
  /* indices*/
  idEmp;
  idCli;
  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];
  tempClientes:  PacienteModel[] = [];
  categorias: CategoriaModel[] = [];
  subcategorias: SubcategoriaModel[] = [];
  cargando = true;

  /* para cargar fichas */
  servicios: ServicioModel[] = [];
  tempServicios:  ServicioModel[] = [];


  /* mostar empleados seleccionados */
  selectedOption: string;
  printedOption: string;

  /* para nombre de clientes */
  selectedOptionName: string;
  printedOptionName: string;

  /* para seleccion de subcategorias */
selectedOptionSub: string;
printedOptionSub: string;



@ViewChild(MatPaginator) paginator: MatPaginator;
  idCat: number;
  catDesc: string;
  idSubcat: number;
  SubcatDesc: string;

  constructor(private auth: AuthService,) { }

  ngOnInit() {
    
    
    /*buscar empleados */
    this.auth.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
    });

    
     /* mostrar las categorias */
     this.auth.getCategoria()
     .subscribe( resp => {
         
       this.categorias = resp;
       
     });
   
    /* cargar servicios */
    this.auth.getServicios()
    .subscribe( resp => {
      this.servicios = resp;
      this.tempServicios = resp;
      this.cargando = false;
     
    });

    this.auth.getClientes()
    .subscribe( resp => {
      this.clientes = resp;
     

    });
    


  }
  buscarCliente(nombreCliente:string){
    
   this.auth.buscarPaciente(nombreCliente)
      .subscribe( resp => { 
        this.clientes = resp;
        this.tempClientes= resp;
      });
  }

  restoreTabla(){
    this.clientes = this.tempClientes;
    
  }


  /* mostar nombres */
  printNombres(name:string){
    this.printedOption = name;
    console.log(name);
  }
  
  printNombreCli(name:string){
    this.printedOptionName = name;
  }

  
  print() {
    this.printedOption = this.selectedOption;
    console.log("My input: ", this.selectedOption);
    
  }
  /* imprimir nombre de clientes */
  printName() {
    this.printedOptionName = this.selectedOptionName;
    console.log("My input: ", this.selectedOptionName);
    
  }

  /*indice de empleado */
  setIdEmp(id:number){
    this.idEmp = id;
   
  }
  /* indice de cliente */
  setIdCli(id:number){
    this.idCli = id;
    
  }

  setDetallesCat(idCategoria: number, descripcion: string ){
    this.idCat = idCategoria;
    this.catDesc = descripcion;
    
  
  }
  pintCatSelect: string;
  printNameCatselect(){
    this.pintCatSelect = this.catDesc;
    this.printSubSelect = '';
    this.mostrarSubcat();
  
  }
  mostrarSubcat(){
  
    /* buscar sucategorias por idCategoria */
      this.auth.buscarSubcategoria(this.idCat)
      .subscribe( resp => { 
        this.subcategorias = resp;
        
      });
    
  
  }
  
  setDetallesSubCat(idTipoProducto: number, descripcion: string ){
    this.idSubcat = idTipoProducto;
    this.SubcatDesc = descripcion;
  }
  
  printSubSelect: string;
  printNameSubselect(){
    this.printSubSelect = this.SubcatDesc;
  
  }

  
  /* guargar una ficha */
  guargarFicha(motivoConsulta,
    diagnostico, observacionidSubcat){
      

      console.log('id elegidos');
      console.log(this.idCli);
     console.log(this.idEmp);

     this.auth.crearFicha(motivoConsulta,
      diagnostico, observacionidSubcat, this.idEmp, this.idCli, this.idSubcat)
      .subscribe(resp =>{
        console.log(resp);
      })

     Swal.fire({
      title: 'La ficha clinica' ,
      text: 'Se guardo correctamente',
      type: 'success'
    });  
  


    }
}
