import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DatePipe } from '@angular/common';
import { FichaClinicaModel } from 'src/app/models/ficha.model';
import { template } from '@angular/core/src/render3';
import Swal from 'sweetalert2';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { PresentacionModel } from 'src/app/models/presentacion.model';

@Component({
  selector: 'app-modificar-servicio',
  templateUrl: './modificar-servicio.component.html',
  styleUrls: ['./modificar-servicio.component.css']
})
export class ModificarServicioComponent implements OnInit {


  cantidad = 1;
  cargando = true;
  fichas: FichaClinicaModel[] = [];
  nombreEmpleado: string;
  nombreCliente: string;
  fecha: Date;
  idFicha: number;
  obser: string;
  categorias: CategoriaModel[] = [];
  idCat: number;
  subcategorias: SubcategoriaModel[] = [];
  catDesc: string;
  idSubcat: number;
  SubcatDesc: string;
  presentacion: PresentacionModel[] = [];

  



  constructor(private auth: AuthService,
              private datePipe: DatePipe) { }

  ngOnInit() {

    this.auth.getFichas()
    .subscribe( resp => {
       this.fichas = resp;
       console.log(resp);
       this.cargando = false;
       /* ordenar ficha por id */
       this.fichas.sort((a,b)=> b.idFichaClinica - a.idFichaClinica);

      
    });

    /* mostrar las categorias */
    this.auth.getCategoria()
    .subscribe( resp => {
        
      this.categorias = resp;
      
    });



  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

  mostarDatosFocha(index: number){

    let tempFicha= new FichaClinicaModel();
    tempFicha = this.fichas[index];
    this.nombreEmpleado = tempFicha.idEmpleado.nombreCompleto;
    this.nombreCliente = tempFicha.idCliente.nombreCompleto;
    this.idFicha = tempFicha.idFichaClinica;
    let dateTemp= this.transformDate(tempFicha.fechaHora);
    this.fecha = new Date(dateTemp);
    

  }

  clear(){
    this.nombreEmpleado = '';
    this.nombreCliente = ''
    this.fecha = undefined;

  }

idServicio: number;
  agregarServicio(obserServicio: string){

    this.auth.setServicio(this.idFicha, obserServicio)
    .subscribe(resp =>{
      this.idServicio = resp;
      Swal.fire({
        title: 'El servicio se ha creado: ',
        text: 'Por favor agregue detalles al servicio',
        type: 'success'
      });  
      console.log(resp);

    })
    
  }

  setDetallesCat(idCategoria: number, descripcion: string ){
    this.idCat = idCategoria;
    this.catDesc = descripcion;
    
  
  }
  pintCatSelect: string;
  printNameCatselect(){
    this.pintCatSelect = this.catDesc;
    this.printSubSelect = '';
    this.mostrarSubcat();
  
  }
  mostrarSubcat(){
  
    /* buscar sucategorias por idCategoria */
      this.auth.buscarSubcategoria(this.idCat)
      .subscribe( resp => { 
        this.subcategorias = resp;
        
      });
    
  
  }
  
  setDetallesSubCat(idTipoProducto: number, descripcion: string ){
    this.idSubcat = idTipoProducto;
    this.SubcatDesc = descripcion;
  }
  
  printSubSelect: string;
  printNameSubselect(){
    this.printSubSelect = this.SubcatDesc;
  
  }

  /* buscar presenacion del producto */
  mostrarPresentacion(){
    this.auth.getPresentacion(this.idSubcat)
    .subscribe(resp =>{
      this.presentacion = resp;
    })
  }

precio: number;
  /* para mostrar el precio del producto */
  setDetallesPres(precio: number ){
    this.precio = precio;

  }
}
