import { Component, OnInit } from '@angular/core';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  categoria: CategoriaModel[] = [];
  cargando = true;
  page  = 1;
  ngDescripcion: string;
  peticion = true;
  idCategoria: number;

  constructor( private auth: AuthService) { }
  

  ngOnInit() {

    this.auth.getCategoria()
    .subscribe( resp =>{
      this.categoria = resp;
      this.categoria.sort((a,b)=> b.idCategoria - a.idCategoria);
      this.cargando = false;
    });

  }

  crearCategoria(descripcion: string){

    /* comprobar si es agregar o actualizar el dato */
    if(this.peticion){

        
      this.auth.setCategoria(descripcion)
      .subscribe(resp =>{
           console.log(resp);
           Swal.fire({
            title: 'La nueva categoria: ' + descripcion,
            text: 'Se guardo correctamente',
            type: 'success'
          }); 
          this.ngOnInit();  
        });

    }


  }

  eliminar(i: number){

    let tempCtegoria = new CategoriaModel();
    tempCtegoria = this.categoria[i];

    Swal.fire({
      title: 'Esta seguro?',
      text: `Esta seguro que desea borrar a ${ tempCtegoria.descripcion}`,
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {

      if ( resp.value ){

        this.categoria.splice( i, 1);
        /*for( var i = 0; i< this.tempClientes.length; i++){
          if (this.tempClientes[i].nombreCompleto === temp.nombreCompleto &&
            this.tempClientes[i].email === temp.email &&
            this.tempClientes[i].telefono === temp.telefono &&
            this.tempClientes[i].tipoCliente === temp.tipoCliente){
              this.tempClientes.splice(i,1);
              break;
            }
        }*/

        this.auth.deleteCategoria(tempCtegoria.idCategoria).subscribe();
        
        

      }

    });
  }

  /* buscar categoria datos */
  buscarCategoria(descripcion: string){
   
    this.auth.getCatId(descripcion).
    subscribe(resp => {

      this.categoria = resp;
      console.log(resp);
      this.categoria.sort((a,b)=> b.idCategoria - a.idCategoria);
      
    })
  }
  


}
