import { Component, OnInit } from '@angular/core';
import { ReservaModel } from 'src/app/models/reserva.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { PacienteModel } from 'src/app/models/paciente.model';
import { DatePipe } from '@angular/common';
import { HorarioAgendaModel } from 'src/app/models/agenda.model';

@Component({
  selector: 'app-crear-reserva',
  templateUrl: './crear-reserva.component.html',
  styleUrls: ['./crear-reserva.component.css']
})
export class CrearReservaComponent implements OnInit {
  cargando = true;
  agendas: HorarioAgendaModel[] = [];
  reservas: ReservaModel[] = [];
  tempReservas: ReservaModel[] = [];
  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];
  page=1;
  p=1;
  idEmp: number;
  nombreEmpleadoSelect: string;
  nombreCliSelect: string;
  idCli: any;
  
  

  constructor( private auth: AuthService, private datePipe: DatePipe) { }

  ngOnInit() {

    /* mostrar agenda */
    this.auth.getAgenda()
    .subscribe(resp =>{

      this.agendas = resp;
      console.log(this.agendas[1].horaApertura);
      this.cargando = false;

    });

    /* mostrar reservar */
    this.auth.mostrarReservas()
    .subscribe(resp =>{
      this.reservas = resp;
      this.tempReservas = resp;
      //console.log(resp);
      this.cargando = false;

      /* ordenar array para mostrar fechas actuales */
      this.reservas.sort((val1, val2)=> {return new Date(val2.fecha).getDate() - new 
        Date(val1.fecha).getDate()});

    });

    /*buscar empleados */
    this.auth.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
    });

    /* buscar empleados */
    this.auth.getClientes()
    .subscribe( resp => {
      this.clientes = resp;
     

    });
  }

  setIdEmp(id: number){
   
    this.idEmp = id;

  } 
  setNameEmp(name: string){

    this.nombreEmpleadoSelect = name;

  }

  printEmpName: string;

  printNameEmpleado(){
    this.printEmpName = this.nombreEmpleadoSelect;

  }

  setIdCli(id){
    this.idCli = id;

  }
  
  setNombreCli(name:string){
    this.nombreCliSelect = name;

  }

  printNameCli: string;

  printNameCliselect(){
    this.printNameCli = this.nombreCliSelect;
  }

  buscarCliente(nombreCliente:string){
    
    this.auth.buscarPaciente(nombreCliente)
       .subscribe( resp => { 
         this.clientes = resp;
       });
   }
   /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

   /* buscar reservas por fechas */

   buscar(fecha){
    console.log(this.transformDate(fecha));
    console.log(this.idEmp);

    this.auth.buscarLibreyNolibreReserva(this.transformDate(fecha), this.idEmp)
    .subscribe(resp =>{
      this.reservas = resp;
    })

    

   }

}
