import { Component, OnInit } from '@angular/core';
import { PresentacionModel } from 'src/app/models/presentacion.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-presentacion-producto',
  templateUrl: './presentacion-producto.component.html',
  styleUrls: ['./presentacion-producto.component.css']
})
export class PresentacionProductoComponent implements OnInit {

  producto: PresentacionModel[] =[];

  constructor( private auth: AuthService ) { }

  ngOnInit() {

    this.auth.getProducto()
    .subscribe(resp =>{
      this.producto = resp;
    })
  }

}
