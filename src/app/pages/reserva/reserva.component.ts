import { Component, OnInit } from '@angular/core';
import { ReservaModel } from 'src/app/models/reserva.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { PacienteModel } from 'src/app/models/paciente.model';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {
  cargando = true;
  reservas: ReservaModel[] = [];
  empleados: UsuarioModel[] = [];
  page = 1;
  idEmp: number;
  nombreEmpleadoSelect: string;
  clientes: PacienteModel[] = [];
  idCli: number;
  nombreCliSelect: string;
  tempReservas: ReservaModel[] = [];


 fechaDesde1:Date;
 fechaHasta1:Date;

  constructor( private auth: AuthService, private datePipe: DatePipe) { }

  ngOnInit() {

    /* mostrar reservar */
    this.auth.mostrarReservas()
    .subscribe(resp =>{
      this.reservas = resp;
      this.tempReservas = resp;
      //console.log(resp);
      this.cargando = false;

      /* ordenar array para mostrar fechas actuales */
      this.reservas.sort((val1, val2)=> {return new Date(val2.fecha).getDate() - new 
        Date(val1.fecha).getDate()});

    });

     /*buscar empleados */
     this.auth.getEmpleado()
     .subscribe( resp => { 
       this.empleados = resp;
     });

     /* buscar empleados */
     this.auth.getClientes()
    .subscribe( resp => {
      this.clientes = resp;
     

    });

    


    
  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

  setIdEmp(id: number){
   
    this.idEmp = id;

  } 
  setNameEmp(name: string){

    this.nombreEmpleadoSelect = name;

  }

  printEmpName: string;

  printNameEmpleado(){
    this.printEmpName = this.nombreEmpleadoSelect;

  }

  setIdCli(id){
    this.idCli = id;

  }
  
  setNombreCli(name:string){
    this.nombreCliSelect = name;

  }

  printNameCli: string;

  printNameCliselect(){
    this.printNameCli = this.nombreCliSelect;
  }


  buscarCliente(nombreCliente:string){
    
    this.auth.buscarPaciente(nombreCliente)
       .subscribe( resp => { 
         this.clientes = resp;
       });
   }

   /* para buscar reservas */
   fechaDesde:string;
   fechaHasta: string;

   buscarReservas(date1, date2){
    
    this.fechaDesde = this.transformDate(date1);
    this.fechaHasta = this.transformDate(date2);

    if(this.idEmp !== undefined){
     
      this.auth.buscarReservasPorFechas(this.fechaDesde, this.fechaHasta, this.idEmp)
    .subscribe( resp => {
      this.cargando = true;
      this.reservas = resp;
      this.cargando = false;
      this.idEmp = undefined;
      
    });

    }else if(this.idCli !== undefined){
      this.auth.buscarReservaPorCliente(this.idCli)
      .subscribe( resp => {
        this.cargando = true;
        this.reservas = resp;
        this.cargando = false;
        this.idCli = undefined;
      });
    }
     
    
     


   }
  

   limpiarPantalla(){
     
     this.cargando = true;
     this.reservas = this.tempReservas;
     this.fechaDesde1 = undefined;
     this.fechaHasta1 = undefined;
     this.printEmpName = '';
     this.printNameCli = '';
     this.cargando = false;
     this.idCli = undefined;
     this.idEmp = undefined;

   }


   /* cancelar reserva */
   cancelarReserva(index: number){

    let temp = new ReservaModel();
    temp = this.reservas[index];
      
    
    Swal.fire({
      title: 'Esta seguro?',
      text: `Esta seguro que desea cancelar la reserva`,
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {

      if ( resp.value ){

        this.reservas.splice( index, 1);
        for( var i = 0; i< this.tempReservas.length; i++){
          if (this.tempReservas[i].fecha === temp.fecha &&
            this.tempReservas[i].horaInicio === temp.horaInicio &&
            this.tempReservas[i].horaFin === temp.horaFin &&
            this.tempReservas[i].idEmpleado.nombreCompleto === temp.idEmpleado.nombreCompleto){
              this.tempReservas.splice(i,1);
              break;
            }
        }

        // this.auth.cancelarReservas(temp.idReserva).subscribe();

      }

    });
    
  }
  
  actualizarReserva = new ReservaModel();
  nombreProfesional: string;
  nameCli: string;
  obs: string;
  asitstido: string;
  flagAsistio = false;
  idReserva : number

  getActualizar( id: number ){

    this.idReserva = id;

    
    this.auth.traerReserva(id)
    .subscribe(resp =>{

      this.actualizarReserva = resp;

      this.actualizarReserva.horaInicio;
      console.log(resp);
      this.nombreProfesional = this.actualizarReserva.idEmpleado.nombreCompleto;
      this.nameCli = this.actualizarReserva.idCliente.nombreCompleto;
      this.obs = this.actualizarReserva.observacion
      this.asitstido = this.actualizarReserva.flagAsistio;
      if(this.asitstido === 'S'){
        this.flagAsistio = true;
      }else{
        this.flagAsistio = false;
      }
      console.log(this.nameCli );
      
      
      console.log(this.actualizarReserva.fecha);

    })

  }

  guargarActualizacion(observacion: string){
    let marcar: string;
    if(this.flagAsistio){
      marcar = 'S';
    }else{
      marcar = null;
    }
    this.obs = observacion;
    console.log(observacion);
   console.log(marcar);
    console.log(this.obs);
    console.log(this.idReserva);
   this.auth.actualizarReserva(this.idReserva, this.obs, marcar)
    .subscribe(resp =>{
      console.log(resp);
       Swal.fire({
       title: 'La reserva',
       text: 'Se actualizo correctamente',
       type: 'success'
     });  
    });
    
    }
  

}
