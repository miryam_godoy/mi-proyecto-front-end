import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ServicioModel } from 'src/app/models/servicio.model';
import { DatePipe } from '@angular/common';
import { CategoriaModel } from 'src/app/models/categoria.model';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { PacienteModel } from 'src/app/models/paciente.model';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.css']
})
export class ServicioComponent implements OnInit {
  page = 1;
  servicios: ServicioModel[] = [];
  tempServicios: ServicioModel[] = [];
  cargando = true;
  categorias: CategoriaModel[] = [];
  subcategorias: SubcategoriaModel[] = [];
  empleados: UsuarioModel[] = [];
  clientes: PacienteModel[] = [];
  nombreEmpleadoSelect: string;
  idEmp: number;
  printedOptionName: string;
  printedOption: string;
  idCli: number | undefined;
  idCat: number;
  catDesc: string;
  idSubcat: number;
  SubcatDesc: string;
  nombreCliente: string;
  inputCliente: string;
  fechaDesde1:Date;
  fechaHasta1:Date;
  constructor(private auth: AuthService,
              private datePipe: DatePipe) { }

  ngOnInit() {
    /* mostrar Servicios */
    this.auth.getServicios()
    .subscribe( resp => {
      this.servicios = resp;
      this.tempServicios = resp;
      //console.log(this.servicios);
      this.cargando = false;
     
    });

     /* mostrar las categorias */
     this.auth.getCategoria()
     .subscribe( resp => {
         
       this.categorias = resp;
       
     });

     /* buscar empleados */
    this.auth.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
    });

    /* buscar empleados */
    this.auth.getClientes()
    .subscribe( resp => {
      this.clientes = resp;
     

    });


  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

  setIdEmp(id: number){
   
    this.idEmp = id;

  } 
  setNameEmp(name: string){

    this.nombreEmpleadoSelect = name;

  }

  printEmpName: string;

  printNameEmpleado(){
    this.printEmpName = this.nombreEmpleadoSelect;

  }

  setIdCli(id){
    this.idCli = id;
    console.log(this.idCli);


  }
  
 
  setNombreCli(name:string){
    this.nombreCliente = name;

  }

  printNameCli: string;

  printNameCliselect(){
    this.printNameCli = this.nombreCliente;
  }

  buscarCliente(nombreCliente:string){
    
    this.auth.buscarPaciente(nombreCliente)
       .subscribe( resp => { 
         this.clientes = resp;
       });
   }
/* fechas */
fechaDesde: Date | undefined;
fechaHasta:Date | undefined;

/* limpiar todo */
limpiarPantalla(){
     
  this.cargando = true;
  this.servicios = this.tempServicios;
  this.fechaDesde1 = undefined;
  this.fechaHasta1 = undefined;
  this.printEmpName = '';
  this.printNameCli = '';
  this.cargando = false;
  this.idCli = undefined;
  this.idEmp = undefined;
  this.idCat = undefined;
  this.catDesc = '';
  this.pintCatSelect = '';

}

setDetallesCat(idCategoria: number, descripcion: string ){
  this.idCat = idCategoria;
  this.catDesc = descripcion;
  

}
pintCatSelect: string;
printNameCatselect(){
  this.pintCatSelect = this.catDesc;
  this.printSubSelect = '';
  this.mostrarSubcat();

}
mostrarSubcat(){

  /* buscar sucategorias por idCategoria */
    this.auth.buscarSubcategoria(this.idCat)
    .subscribe( resp => { 
      this.subcategorias = resp;
      
    });
  

}

setDetallesSubCat(idTipoProducto: number, descripcion: string ){
  this.idSubcat = idTipoProducto;
  this.SubcatDesc = descripcion;
}

printSubSelect: string;
printNameSubselect(){
  this.printSubSelect = this.SubcatDesc;

}

buscar(fecha1: Date, fecha2: Date){

  console.log(this.transformDate(fecha1));

  
  if( this.transformDate(fecha1) !== null && this.transformDate(fecha2)  !== null ){
    console.log('pedi fecha');
    this.auth.getServicioFechas(this.transformDate(fecha1), this.transformDate(fecha2))
    .subscribe( resp => {
     this.servicios= resp;
    });

  }else if( this.idCli !== undefined ){
    console.log('pedi pacientes');
    this.auth.getServicioPaciente(this.idCli)
    .subscribe(resp => {
      this.servicios = resp;
      console.log(this.servicios);
    });

  }else if( this.idEmp !== undefined){
    console.log('pedi empleados');
    this.auth.getServicoEmpleados(this.idEmp)
    .subscribe(resp => {
      this.servicios = resp;
      console.log(this.servicios);
    });
  }/*else if( this.idSubcat !== undefined){
    console.log('pedi tipoProducto');
    console.log(this.idSubcat);
    this.auth.getServicioSubcategoria(this.idSubcat)
    .subscribe(resp => {
      this.servicios = resp;
      console.log(this.servicios);
    });
  
  }*/
  
  

}


}
