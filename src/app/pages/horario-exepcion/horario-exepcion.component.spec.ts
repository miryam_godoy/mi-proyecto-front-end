import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorarioExepcionComponent } from './horario-exepcion.component';

describe('HorarioExepcionComponent', () => {
  let component: HorarioExepcionComponent;
  let fixture: ComponentFixture<HorarioExepcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorarioExepcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioExepcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
