import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { NgForm } from '@angular/forms';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { ServicioModel } from 'src/app/models/servicio.model';
import { FichaClinicaModel } from 'src/app/models/ficha.model';
import { DatePipe } from '@angular/common';
import { PacienteModel } from 'src/app/models/paciente.model';
import { CategoriaModel } from 'src/app/models/categoria.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

  
  
})
export class HomeComponent implements OnInit {

page = 1
   cargando = true;
  

  empleados: UsuarioModel[] = [];

  clientes: PacienteModel[] = [];

  nombre: string;


 nombreCliente: string;
inputCliente: string;
fechaDesde1:Date;
fechaHasta1:Date;



  subcategorias: SubcategoriaModel[] = [];


  /* para mostrar los servicios */

  fichas: FichaClinicaModel[] = [];
  tempFichas: FichaClinicaModel[] = [];
  categorias: CategoriaModel[] = [];
  nombreEmpleadoSelect: string;
  idEmp: number;
  printedOptionName: string;
  printedOption: string;
  idCli: number | undefined;
  idCat: number;
  catDesc: string;
  idSubcat: number;
  SubcatDesc: string;
  

  constructor( private auth: AuthService,
               private router: Router, private datePipe: DatePipe) { }

  ngOnInit() {

    /* buscar los servicios existentes*/
    this.auth.getFichas()
    .subscribe( resp => {
        
      this.fichas = resp;
      this.tempFichas = resp;
      this.cargando = false;
      
    
    });


    /* mostrar las categorias */
    this.auth.getCategoria()
    .subscribe( resp => {
        
      this.categorias = resp;
      
    });

    /* buscar empleados */
    this.auth.getEmpleado()
    .subscribe( resp => { 
      this.empleados = resp;
    });

    /* buscar empleados */
    this.auth.getClientes()
    .subscribe( resp => {
      this.clientes = resp;
     

    });
    


  
    
 
  }

  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyyMMdd');
  }

  setIdEmp(id: number){
   
    this.idEmp = id;

  } 
  setNameEmp(name: string){

    this.nombreEmpleadoSelect = name;

  }

  printEmpName: string;

  printNameEmpleado(){
    this.printEmpName = this.nombreEmpleadoSelect;

  }

  setIdCli(id){
    this.idCli = id;
    console.log(this.idCli);


  }
  
 
  setNombreCli(name:string){
    this.nombreCliente = name;

  }

  printNameCli: string;

  printNameCliselect(){
    this.printNameCli = this.nombreCliente;
  }


  salir(){

    this.auth.logout();
    this.router.navigateByUrl('/login');

  }

  buscarCliente(nombreCliente:string){
    
    this.auth.buscarPaciente(nombreCliente)
       .subscribe( resp => { 
         this.clientes = resp;
       });
   }

  
  
 
  onSubmit( form: NgForm ){

    if ( form.invalid ) { return ; }

   // localStorage.setItem('name', this.nombreCliente.nombreCompleto);


  }

  /* prueba */

  textValue = 'initial value';
  log = '';

  logText(value: string): void {
    this.log += `Text changed to '${value}'\n`;
    console.log(this.log);
  }

  

  


/* fechas */
fechaDesde: Date | undefined;
fechaHasta:Date | undefined;

/* limpiar todo */
limpiarPantalla(){
     
  this.cargando = true;
  this.fichas = this.tempFichas;
  this.fechaDesde1 = undefined;
  this.fechaHasta1 = undefined;
  this.printEmpName = '';
  this.printNameCli = '';
  this.cargando = false;
  this.idCli = undefined;
  this.idEmp = undefined;
  this.idCat = undefined;
  this.catDesc = '';
  this.pintCatSelect = '';

}

setDetallesCat(idCategoria: number, descripcion: string ){
  this.idCat = idCategoria;
  this.catDesc = descripcion;
  

}
pintCatSelect: string;
printNameCatselect(){
  this.pintCatSelect = this.catDesc;
  this.printSubSelect = '';
  this.mostrarSubcat();

}
mostrarSubcat(){

  /* buscar sucategorias por idCategoria */
    this.auth.buscarSubcategoria(this.idCat)
    .subscribe( resp => { 
      this.subcategorias = resp;
      
    });
  

}

setDetallesSubCat(idTipoProducto: number, descripcion: string ){
  this.idSubcat = idTipoProducto;
  this.SubcatDesc = descripcion;
}

printSubSelect: string;
printNameSubselect(){
  this.printSubSelect = this.SubcatDesc;

}

buscar(fecha1: Date, fecha2: Date){

  console.log(this.transformDate(fecha1));

  
  if( this.transformDate(fecha1) !== null && this.transformDate(fecha2)  !== null ){
    console.log('pedi fecha');
    this.auth.getFichasFechas(this.transformDate(fecha1), this.transformDate(fecha2))
  .subscribe( resp => {
    this.fichas = resp;
  });

  }else if( this.idCli !== undefined ){
    console.log('pedi pacientes');
    this.auth.getFichasPaciente(this.idCli)
    .subscribe(resp => {
      this.fichas = resp;
      console.log(this.fichas);
    });

  }else if( this.idEmp !== undefined){
    console.log('pedi empleados');
    this.auth.getFichasEmpleados(this.idEmp)
    .subscribe(resp => {
      this.fichas = resp;
      console.log(this.fichas);
    });
  }else if( this.idSubcat !== undefined){
    console.log('pedi tipoProducto');
    console.log(this.idSubcat);
    this.auth.getFichasSubcategoria(this.idSubcat)
    .subscribe(resp => {
      this.fichas = resp;
      console.log(this.fichas);
    });
  
  }
  
  

}







}




