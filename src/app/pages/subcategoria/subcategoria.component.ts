import { Component, OnInit } from '@angular/core';
import { SubcategoriaModel } from 'src/app/models/subcategoria.model';
import { AuthService } from 'src/app/services/auth.service';
import { CategoriaModel } from 'src/app/models/categoria.model';

@Component({
  selector: 'app-subcategoria',
  templateUrl: './subcategoria.component.html',
  styleUrls: ['./subcategoria.component.css']
})
export class SubcategoriaComponent implements OnInit {

  subcategoria: SubcategoriaModel[] = [];
  categoria: CategoriaModel[] = [];
 page=1;
  cargando = true;
  formGroup: any;

  constructor( private auth: AuthService ) { }

  ngOnInit() {

    this.auth.getSubcategoria()
    .subscribe(resp => {
      this.subcategoria = resp;
      this.subcategoria.sort((a,b)=> b.idTipoProducto - a.idTipoProducto);
      this.cargando = false;

    });
    this.auth.getCategoria()
    .subscribe(resp =>{
      this.categoria = resp;
      this.categoria.sort((a,b)=> b.idCategoria - a.idCategoria);
      
    });
   
  }

  guardarCat(idCategoria: number){
    console.log('categoria');
    console.log(idCategoria);
  }

}
