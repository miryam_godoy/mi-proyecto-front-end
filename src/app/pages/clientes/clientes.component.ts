import { Component, OnInit } from '@angular/core';
import { PacienteModel } from 'src/app/models/paciente.model';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { PagerService } from 'src/app/services/pager.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  page:number = 1;

  peticion = false;

  clientes: PacienteModel[]=[];
  tempClientes: PacienteModel[]=[];
  cargando = true;
  dataUpdate = new PacienteModel(); /* para cuando se quiera actualizar */

  constructor( private auth: AuthService, 
              private datePipe: DatePipe,)
     { }

    

  ngOnInit() {

    /* para mostrar clientes en la tabla */
    this.auth.getClientes()
        .subscribe( resp => {
          
          this.sleep(1000);
          this.clientes = resp;
          this.tempClientes = resp;
          this.cargando = false;
           
           
          
        
        });
  
  }

  /* tiempo de espera */
  sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > milliseconds){
        break;
      }
    }
  }

  
  /* convertir fecha */
  transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
  }
  /* buscar cliente */
  buscarCliente(nombreCliente:string){
    this.cargando = true;
    
    
   this.auth.buscarPaciente(nombreCliente)
      .subscribe( resp => { 
        this.clientes = resp;
        this.cargando = false;
       // console.log(this.clientes);
        
      });
  }

  /* para restablecer la tabla */

  restoreTabla(){
    this.cargando = true;
    this.clientes = this.tempClientes;
    this.cargando = false;
  }
  /* guargar clientes */
  guardarCliente( 
    cedula:string, ruc: string, telefono:string, tipoPersona: string
    ,nombre: string, apellido: string, fechaNacimiento: Date, email: string){

      
      console.log(this.dataUpdate);
      let fecha = this.transformDate(fechaNacimiento);
      console.log(fecha);

      let personaTipo = tipoPersona.toLocaleUpperCase();

      Swal.fire({
        title: 'Espere',
        text: 'Guardando informacion',
        type:'info',
        allowOutsideClick: false
      });
      Swal.showLoading();
      

      /* comprobar si es agregar o actualizar el dato */
      if(this.peticion){

        
        this.auth.guardarPaciente( 
          cedula, ruc, telefono, personaTipo
          ,nombre, apellido,fecha, email).subscribe(resp =>{
             console.log(resp);
             Swal.fire({
              title: 'El nuevo cliente: ' + nombre,
              text: 'Se guardo correctamente',
              type: 'success'
            });  
          });

      }else{
        
        this.auth.actualizarCliente(this.dataUpdate.idPersona,cedula, ruc, telefono, personaTipo
          ,nombre, apellido,fecha, email).subscribe(resp =>{
            console.log(resp);
            Swal.fire({
             title: 'El cliente: ' + nombre,
             text: 'Se actualizo correctamente',
             type: 'success'
           });  
         });

      }

      
       console.log(this.peticion);

        this.inicializar();

        
    }
    /* para actualizar */
    actualizar(item: number){

      let temp = new PacienteModel();
      temp =this.clientes[item];
      this.auth.mostrarCliente(temp.idPersona)
      .subscribe( resp => {
        console.log(resp);
        this.dataUpdate = resp;
        
        });
        console.log(this.peticion);

    }

    /* para eliminar */
    borrar( item:number){
      console.log(this.clientes[item]);
      /* borrar de tempCliente y clientes los elementos */
      
      let index = 0;
      
      let temp = new PacienteModel();
      temp =this.clientes[item];

       let j = temp.idPersona;
       console.log(temp.idPersona);
       

      Swal.fire({
        title: 'Esta seguro?',
        text: `Esta seguro que desea borrar a ${ temp.nombreCompleto }`,
        type: 'question',
        showConfirmButton: true,
        showCancelButton: true
      }).then(resp => {
  
        if ( resp.value ){
  
          this.clientes.splice( item, 1);
          for( var i = 0; i< this.tempClientes.length; i++){
            if (this.tempClientes[i].nombreCompleto === temp.nombreCompleto &&
              this.tempClientes[i].email === temp.email &&
              this.tempClientes[i].telefono === temp.telefono &&
              this.tempClientes[i].tipoCliente === temp.tipoCliente){
                this.tempClientes.splice(i,1);
                break;
              }
          }
  
          this.auth.borrarCliente(temp.idPersona).subscribe();
          

  
  
        }
  
      });
      
    }

    
inicializar(){
  this.dataUpdate = new PacienteModel();
}

}
