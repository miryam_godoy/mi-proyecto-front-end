import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { CategoriaModel } from '../models/categoria.model';
import { stringify } from '@angular/compiler/src/util';
import { SubcategoriaModel } from '../models/subcategoria.model';
import { PacienteModel } from '../models/paciente.model';
import { FichaClinicaModel } from '../models/ficha.model';
import { ServicioModel } from '../models/servicio.model';
import { ReservaModel } from '../models/reserva.model';
import { HorarioAgendaModel } from '../models/agenda.model';
import { PresentacionModel } from '../models/presentacion.model';




@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  private username: string;

  private urlTp = 'https://gy7228.myfoscam.org:8443/stock-pwfe';

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';

  private apiKey = 'AIzaSyC9BCBu60WdC3rZbKNIC9vDOFa-Y_V9Pd8';

  userToken: string;



  //crear nuevo usuario
  //https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  //login
  //https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]



  constructor(private http: HttpClient) {
    this.leerToken();
  }


  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }


  logout() {

    this.loggedIn.next(false);
    localStorage.removeItem('token');


  }

  login(usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    }


    return this.http.post(`${this.url}signInWithPassword?key=${this.apiKey}`, authData)
      .pipe(
        map(resp => {
          console.log('Entro al mapa del RXJS');
          this.guardarToken(resp['idToken']);
          this.loggedIn.next(true);
          this.username = authData.nombre;
          return resp;
        })
      );


  }

  nuevoUsuario(usuario: UsuarioModel) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    }

    return this.http.post(`${this.url}signUp?key=${this.apiKey}`, authData)
      .pipe(
        map(resp => {
          console.log('Entro al mapa del RXJS');
          this.guardarToken(resp['idToken']);
          return resp;
        })
      );

  }

  private guardarToken(idToken: string) {

    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds(3600);
    localStorage.setItem('expira', hoy.getTime().toString());
  }

  leerToken() {
    if (localStorage.getItem('token')) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }


  estaAutenticado(): boolean {

    if (this.userToken.length < 2) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));

    const expiraDate = new Date();

    expiraDate.setTime(expira);

    if (expiraDate > new Date()) {
      return true;
    } else {
      return true;
    }




  }


  getCategoria() {
    return this.http.get(`${this.urlTp}/categoria`)
      .pipe(
        map(this.crearArreglo),

      );
  }

  setCategoria(descripcion: string){
    const body = {
      "descripcion": descripcion,
      
    };

    return this.http.post(`${this.urlTp}/categoria`, body)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );

  }

  getCatId(descripcion:string){

    let params = new HttpParams();
    params = params.append('ejemplo',`{"descripcion":${descripcion}}`);



    return this.http.get(`${this.urlTp}/categoria`, { params: params })
    .pipe(
      map(this.crearArreglo),

    );

  }

  actualizarCategoria(id, descripcion){

    //const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };


    /* body */
    const body = {
      "idCategoria": id,
      "descripcion": descripcion,
      
     
    };
  

    return this.http.put(`${this.urlTp}/categoria`, body)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );

  }

  deleteCategoria(idCategoria: number){

    return this.http.delete(`${this.urlTp}/categoria/${idCategoria}`);


  }

  private crearArreglo(categoriasObj) {

    const categorias: CategoriaModel[] = [];

    //console.log('lo que trae del puto array');
    //console.log(categoriasObj);


    if (categoriasObj === null) { return []; }

    categoriasObj.lista.forEach((item) => {

      const categoria = new CategoriaModel();
      categoria.idCategoria = item.idCategoria;
      categoria.descripcion = item.descripcion;
      categoria.flagVisible = item.flagVisible;
      categoria.posicion = item.posicion;

      categorias.push(categoria);
    });

    return categorias;

  }

  getEmpleado() {

    let params = new HttpParams();
    params = params.append('ejemplo', '{"soloUsuariosDelSistema":true}');


    return this.http.get(`${this.urlTp}/persona`, { params: params })
      .pipe(
        map(this.crearArregloPersona),

      );
  }


  /* para buscar clientes por nombres */

  buscarCliente(nombreCliente: string) {

    let params = new HttpParams();
    params = params.append('ejemplo', `{"nombre": "${nombreCliente}"}`);



    return this.http.get(`${this.urlTp}/persona`, { params: params })
      .pipe(
        map(this.crearArregloPersona),

      );
  }


  /* crear arreglo para  retornar personas */
  private crearArregloPersona(personaObj) {

    const personas: UsuarioModel[] = [];

    if (personaObj === null) { return []; }

    personaObj.lista.forEach((item) => {
      
      const persona = new UsuarioModel();
      
      persona.idPersona = item.idPersona;
      persona.nombre = item.nombre;
      persona.nombreCompleto = item.nombreCompleto;
      persona.email = item.email;
      persona.password = item.usuarioLogin;
     
      personas.push(persona);
    });

    return personas;

  }

  getUsername() {
    return this.username;
  }

  /* buscar subcategoria */

  buscarSubcategoria(idCategoria: number) {

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCategoria":{"idCategoria": ${idCategoria}}}`);



    return this.http.get(`${this.urlTp}/tipoProducto`, { params: params })
      .pipe(
        map(this.crearArregloSubcategoria),

      );
  }

  getSubcategoria(){

    return this.http.get(`${this.urlTp}/tipoProducto`)
      .pipe(
        map(this.crearArregloSubcategoria),

      );

  }

  /* retornar array de subcategorias */
  private crearArregloSubcategoria(subCatObj) {

    const subcategorias: SubcategoriaModel[] = [];

    if (subCatObj === null) { return []; }

    subCatObj.lista.forEach((item) => {
      
      const sub = new SubcategoriaModel();

      sub.idTipoProducto = item.idTipoProducto
      sub.descripcion = item.descripcion;
      
      subcategorias.push(sub);
    });

    return subcategorias;

  }

  /* mostrar cliente */
  getClientes() {
    return this.http.get(`${this.urlTp}/persona`)
      .pipe(
        map(this.crearArregloPaciente),

      );
  }

  /* crear arreglo paciente */

  private crearArregloPaciente(pacCatObj) {

    const pacientes: PacienteModel[] = [];

    if (pacCatObj === null) { return []; }

    pacCatObj.lista.forEach((item) => {
      
      const paciente = new PacienteModel();
      
      paciente.idPersona = item.idPersona;
      paciente.nombreCompleto = item.nombreCompleto;
      paciente.email = item.email;
      paciente.telefono = item.telefono;
      paciente.tipoCliente = item.tipoCliente;
     
      pacientes.push(paciente);
    });

    return pacientes;

  }

  /* buscar paciente */
  buscarPaciente(nombreCliente: string) {

    let params = new HttpParams();
    params = params.append('ejemplo', `{"nombre": "${nombreCliente}"}`);



    return this.http.get(`${this.urlTp}/persona`, { params: params })
      .pipe(
        map(this.crearArregloPaciente),

      );


  }

  /* guardar paciente */
  guardarPaciente(
    cedula: string, ruc: string, telefono: string, tipoPersona: string
    , nombre: string, apellido: string, fechaNacimiento: string, email: string) {


    const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    /* body */
    const obj = {
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "telefono": telefono,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento,

    };
    

    return this.http.post(`${this.urlTp}/persona`, obj, headers)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );



  }
  
  /* borrar clientes */
  borrarCliente(id: number) {

    return this.http.delete(`${this.urlTp}/persona/${id}`);

  }
  /* buscar fichas por fechas */
  getFichasFechas(fechaDesde: string, fechaHasta: string){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"fechaDesdeCadena":"${fechaDesde}","fechaHastaCadena":"${fechaHasta}"}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }
  /* buscar fichas por paciente */
  getFichasPaciente(idCli){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCliente":{"idPersona":${idCli}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* buscar fichas por empleados */

  getFichasEmpleados(idEmp){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idEmpleado":{"idPersona":${idEmp}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }
  /* buscar fichas por tipo de producto */
  getFichasSubcategoria(idSubcat){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idTipoProducto":{"idTipoProducto":${idSubcat}}}`);

    return this.http.get(`${this.urlTp}/fichaClinica`,{ params: params })
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* mostrar agenda */
  getAgenda(){

    return this.http.get(`${this.urlTp}/personaHorarioAgenda`)
      .pipe(
        map(this.crearArregloAgenda),

      );

  }
  /* crear arreglo agenda */
  private crearArregloAgenda(agendaObj) {

    const horarios: HorarioAgendaModel[] = [];

    console.log(agendaObj);

    if (agendaObj === null) { return []; }

    agendaObj.lista.forEach((item) => {
     
      const agenda = new HorarioAgendaModel();
      agenda.idPersonaHorarioAgenda = item.idPersonaHorarioAgenda;
      console.log(agenda.idPersonaHorarioAgenda);
      agenda.horaApertura = item.horaApertura;
      agenda.horaCierre = item.horaCierre;
      agenda.idEmpleado = item.idEmpleado;
      horarios.push(agenda);
    });


    return horarios;

  }



  /* mostrar ficha */
  getFichas() {

    return this.http.get(`${this.urlTp}/fichaClinica`)
      .pipe(
        map(this.crearArregloFicha),

      );

  }

  /* crear areglo para fichas clinicas */
  private crearArregloFicha(ficCatObj) {

    const fichas: FichaClinicaModel[] = [];

    //console.log(ficCatObj);

    if (ficCatObj === null) { return []; }

    ficCatObj.lista.forEach((item) => {
     
      const ficha = new FichaClinicaModel();
      ficha.idFichaClinica = item.idFichaClinica;
      ficha.fechaHora = item.fechaHora;
      ficha.fechaHoraCadena = item.fechaHoraCadena;
      ficha.diagnostico = item.diagnostico;
      ficha.motivoConsulta = item.motivoConsulta;
      ficha.observacion = item.observacion;
      ficha.idEmpleado = item.idEmpleado;
      ficha.idCliente = item.idCliente;
      ficha.idTipoProducto = item.idTipoProducto;
      fichas.push(ficha);
    });


    return fichas;

  }
  /* mostrar sevicios */
  getServicios() {

    return this.http.get(`${this.urlTp}/servicio`)
      .pipe(
        map(this.crearArregloServicio),

      );

  }
   /* crear areglo para fichas clinicas */
   private crearArregloServicio(servObj) {

    const servicios: ServicioModel[] = [];

    //console.log(ficCatObj);

    if (servObj === null) { return []; }

    servObj.lista.forEach((item) => {
     
      const servicio = new ServicioModel();
      servicio.idServicio = item.idServicio;
      servicio.fechaHora = item.fechaHora;
      servicio.presupuesto = item.presupuesto;
      servicio.idFichaClinica = item.idFichaClinica;
      servicio.idEmpleado = item.idEmpleado;
      servicio.idTipoProducto = item.idTipoProducto;
      servicio.fechaHoraCadena = item.fechaHoraCadena;
      servicios.push(servicio);
    });

    

    return servicios;


  }


  mostrarCliente(id: number){

    return this.http.get<PacienteModel>(`${this.urlTp}/persona/${id}`)
    
  }

  /* actualizar clientes */
  actualizarCliente(id:number,cedula, ruc, telefono, tipoPersona
    ,nombre, apellido,fechaNacimiento, email){

      const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    /* body */
    const obj = {
      "idPersona": id,
      "nombre": nombre,
      "apellido": apellido,
      "email": email,
      "telefono": telefono,
      "ruc": ruc,
      "cedula": cedula,
      "tipoPersona": tipoPersona,
      "fechaNacimiento": fechaNacimiento,

    };
  

    return this.http.put(`${this.urlTp}/persona`, obj, headers)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );

    }

  

  /* guardar fichas nuevas */

  crearFicha(motivoConsulta,
    diagnostico, observacionidSubcat, idEmp, idCli, idTipoProducto){
      let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      headers = headers.append('usuario', 'gustavo');
        const body={
           "motivoConsulta":motivoConsulta ,
           "diagnostico":diagnostico,
           "observacion":observacionidSubcat, 
           "idEmpleado":
           { "idPersona": idEmp },
           "idCliente":
           { "idPersona": idCli }, 
           "idTipoProducto": 
           { "idTipoProducto":idTipoProducto }

        };
        return this.http.post(`${this.urlTp}/fichaClinica`, body, {headers})
      .pipe(
         map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );


    }

    traerReserva(idReserva: number){
      return this.http.get<ReservaModel>(`${this.urlTp}/reserva/${idReserva}`);
        
    }

  mostrarReservas(){

    return this.http.get(`${this.urlTp}/reserva`)
        .pipe(
          map(this.crearArregloReserva),
  
        );


  }
  buscarReservasPorFechas(fechaDesde, fechaHasta, idEmpleado){

    let params = new HttpParams();
    params = params.append('ejemplo', `{"idEmpleado":{"idPersona": ${idEmpleado}},
    "fechaDesdeCadena":"${fechaDesde}","fechaHastaCadena":"${fechaHasta}"}`);

    return this.http.get(`${this.urlTp}/reserva`, { params: params })
        .pipe(
          map(this.crearArregloReserva),
  
        );

  }
  crearArregloReserva(reservaObj){
    const reservas: ReservaModel[] = [];

    

    if (reservaObj === null) { return []; }

    reservaObj.lista.forEach((item) =>{
      const reserva = new ReservaModel();
      reserva.idReserva = item.idReserva;
      reserva.observacion = item.observacion;
      reserva.flagAsistio = item.flagAsistio;
      reserva.fecha = item.fecha
      reserva.horaInicio = item.horaInicio;
      reserva.horaFin = item.horaFin;
      reserva.idEmpleado = item.idEmpleado;
      reserva.idCliente = item.idCliente;
      reservas.push(reserva);

    });
    return reservas;
  }

  buscarReservaPorCliente(idCli){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"idCliente":{"idPersona":${idCli}}}`);

    return this.http.get(`${this.urlTp}/reserva`,{ params: params })
        .pipe(
          map(this.crearArregloReserva),
  
        );

  }

  buscarLibreyNolibreReserva(fecha, idEmp){
    let params = new HttpParams();
    params = params.append('fecha', `${fecha}`);

    return this.http.get(`${this.urlTp}/persona/${idEmp}/agenda`,{ params: params })
    .pipe(
      map(this.crearArregloReservaLibreyNo),

    );
        

  }
  private crearArregloReservaLibreyNo(reservaObj){
    const reservas: ReservaModel[] = [];
     if (reservaObj === null){ return []; }

     Object.keys(reservaObj).forEach((key) =>{
        const reserva = new ReservaModel();
        reserva.horaInicio = reservaObj[key].horaInicio;
        reserva.horaFin = reservaObj[key].horaFin;
        if ( reservaObj[key].idCliente === null ){

          reserva.idCliente = undefined;
        }
        
        
        reservas.push(reserva);
     })
     return reservas;
  }

  /* eliminar reservas */
  cancelarReservas(id: number){
    

      return this.http.delete(`${this.urlTp}/reserva/${id}`);
  
    
  }

  actualizarReserva(idReserva:number, observacion:string, marcar: string){

    const headers = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

    /* body */
    const body = {
      "idReserva": idReserva, 
      "observacion": observacion,
      "flagAsistio": marcar
    };

    console.log(body);
  

    return this.http.put(`${this.urlTp}/reserva`, body, headers)
      .pipe(
        map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );
    

  }

  /* buscar servicios por fechas */
  getServicioFechas(fechaDesde: string, fechaHasta: string){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"fechaDesdeCadena":"${fechaDesde}","fechaHastaCadena":"${fechaHasta}"}`);

    return this.http.get(`${this.urlTp}/servicio`,{ params: params })
      .pipe(
        map(this.crearArregloServicio),

      );

  }

  /* buscar servicio por paciente */
  getServicioPaciente(idCli: number){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"idFichaClinica":{"idCliente":{"idPersona":${idCli}}}}`);

    return this.http.get(`${this.urlTp}/servicio`,{ params: params })
        .pipe(
          map(this.crearArregloServicio),
  
        );


  }

  /* buscar servicio por empleado */
  getServicoEmpleados(idEmp: number){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"idEmpleado":{"idPersona":${idEmp}}}`);

    return this.http.get(`${this.urlTp}/servicio`,{ params: params })
        .pipe(
          map(this.crearArregloServicio),
  
        );

  }

  /* para crear servicio */
  setServicio(idFicha: number, obserServicio: string){

    let headers: HttpHeaders = new HttpHeaders();
      headers = headers.append('Content-Type', 'application/json');
      headers = headers.append('usuario', 'gustavo');
        const body={
          "idFichaClinica": { 
          "idFichaClinica":idFicha
         }, 
          "observacion":obserServicio,

        };
        return this.http.post(`${this.urlTp}/servicio`, body, {headers})
      .pipe(
         map((resp: any) => {

          console.log(resp);
          return resp;
        })
      );
    


  }

  getProducto(){

    return this.http.get(`${this.urlTp}/presentacionProducto`)
    .pipe(
      map(this.crearArregloPresentacion),

    );

  }



  getPresentacion(idSubcat: number){
    let params = new HttpParams();
    params = params.append('ejemplo', `{"idProducto":{"idTipoProducto":{"idTipoProducto":${idSubcat}}}}`);

    return this.http.get(`${this.urlTp}/presentacionProducto`,{ params: params })
        .pipe(
          map(this.crearArregloPresentacion),
  
        );

  }

  crearArregloPresentacion(presObj){
    const presentaciones: PresentacionModel[] = [];

    

    if (presObj === null) { return []; }

    presObj.lista.forEach((item) =>{
      const presentacion = new PresentacionModel();
      presentacion.idPresentacionProducto = item.idPresentacionProducto;
      presentacion.descripcion = item.descripcion;
      presentacion.existenciaProducto = item.existenciaProducto;
      presentaciones.push(presentacion);

    });
    return presentaciones;
  }






  
}
