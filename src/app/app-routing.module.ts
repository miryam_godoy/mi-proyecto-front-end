import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { RegistroComponent } from './pages/registro/registro.component';
import { LoginComponent } from './pages/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { NuevaFichaComponent } from './pages/nueva-ficha/nueva-ficha.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { ReservaComponent } from './pages/reserva/reserva.component';
import { CrearReservaComponent } from './pages/crear-reserva/crear-reserva.component';
import { ModificarServicioComponent } from './pages/modificar-servicio/modificar-servicio.component';
import { ServicioComponent } from './pages/servicio/servicio.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { SubcategoriaComponent } from './pages/subcategoria/subcategoria.component';
import { PresentacionProductoComponent } from './pages/presentacion-producto/presentacion-producto.component';
import { HorarioAtencionComponent } from './pages/horario-atencion/horario-atencion.component';
import { HorarioExepcionComponent } from './pages/horario-exepcion/horario-exepcion.component';

const routes: Routes = [
  { path: 'administracion/categoria'   , component: CategoriaComponent, canActivate: [ AuthGuard ] },
  { path: 'administracion/subcategoria'   , component: SubcategoriaComponent, canActivate: [ AuthGuard ] },
  { path: 'administracion/presentacionProducto'   , component: PresentacionProductoComponent, canActivate: [ AuthGuard ] },
  { path: 'empleado/horario-atencion'   , component: HorarioAtencionComponent, canActivate: [ AuthGuard ] },
  { path: 'empleado/horario-exepcion'   , component: HorarioExepcionComponent, canActivate: [ AuthGuard ] },

  { path: 'ficha'    , component: HomeComponent, canActivate: [ AuthGuard ] },
  { path: 'registro', component: RegistroComponent },
  { path: 'login'   , component: LoginComponent },
  { path: 'ficha/agregar-ficha'   , component: NuevaFichaComponent, canActivate: [ AuthGuard ] },
  { path: 'pacientes'   , component: ClientesComponent, canActivate: [ AuthGuard ] },
  { path: 'reserva'   , component: ReservaComponent, canActivate: [ AuthGuard ] },
  { path: 'reserva/agregar-reserva'   , component: CrearReservaComponent, canActivate: [ AuthGuard ] },
  { path: 'servicios'   , component: ServicioComponent, canActivate: [ AuthGuard ] },
  { path: 'servicios/crear-modificar-servicio'   , component: ModificarServicioComponent, canActivate: [ AuthGuard ] },
 
  { path: '**', redirectTo: 'registro' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
